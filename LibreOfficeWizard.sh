#!/bin/bash
load_libs() {
    source ./lib/parameters.sh
    source ./lib/download.sh
    source ./lib/menu.sh
    source ./lib/dialog.sh
    source ./lib/tools.sh
    source ./lib/config.sh
    source ./lib/autogen.sh
    source ./lib/about.sh
    source ./lib/install.sh
    source ./lib/build.sh
    source ./lib/clean.sh
    source ./lib/quick_action.sh
    source ./lib/run.sh
}

startup() {
    clear

    load_libs

    load_config
    create_tmp_file
    run_apt_update
    sleep $SLEEP_TIME
}

main() {
    startup

    low_main_menu

    quit
}

quit() {
    clear

    case $RESULT in
    0)
        success "It was a pleasure!\nExit: Quit"
        ;;
    1)
        warning "It was a pleasure!\nExit: Canceled"
        ;;
    255)
        danger "It was a pleasure!\nExit: Escaped"
        ;;
    esac
}

main
