success() {
    local -r MSG="$@"

    if [[ "$(is_empty_str "${MSG}")" = 'false' ]]; then
        echo -e "\e[1;32m${MSG}\e[0m" 2>&1
    fi
}

info() {
    local -r MSG="$@"

    if [[ "$(is_empty_str "${MSG}")" = 'false' ]]; then
        echo -e "\e[1;34m${MSG}\e[0m" 2>&1
    fi
}

warning() {
    local -r MSG="$@"

    if [[ "$(is_empty_str "${MSG}")" = 'false' ]]; then
        echo -e "\e[1;33m${MSG}\e[0m" 2>&1
    fi
}

danger() {
    local -r MSG="$@"

    if [[ "$(is_empty_str "${MSG}")" = 'false' ]]; then
        echo -e "\e[1;31m${MSG}\e[0m" 2>&1
    fi
}

trim_str() {
    local -r STR="${1}"

    sed 's,^[[:blank:]]*,,' <<<"${STR}" | sed 's,[[:blank:]]*$,,'
}

is_empty_str() {
    local -r STR="${1}"

    if [[ "$(trim_str "${STR}")" = '' ]]; then
        echo 'true' && return 0
    fi

    echo 'false' && return 1
}

get_last_apt_update() {
    local APT="$(stat -c %Y '/var/cache/apt')"
    local NOW="$(date +'%s')"

    echo $((NOW - APT))
}

run_apt_update() {
    local UPDATE_INTERVAL="${1}"
    local LAST_APT_UPDATE="$(get_last_apt_update)"

    if [[ "$(is_empty_str "${UPDATE_INTERVAL}")" = 'true' ]]; then
        # Default To 24 hours
        UPDATE_INTERVAL="$((24 * 60 * 60))"
    fi

    if [[ "${LAST_APT_UPDATE}" -gt "${UPDATE_INTERVAL}" ]]; then
        run_command "sudo apt update -m"

        return 0
    else
        local LAST_UPDATE="$(date -u -d @"${LAST_APT_UPDATE}" +'%-Hh %-Mm %-Ss')"
        info "Skip apt-get update because its last run was '${LAST_UPDATE}' ago"

        return 1
    fi
}

run_command() {
    local CMD=${1}

    info "$@"
    shift
    eval $CMD $@
    read -p "$PROMT"
    clear
}

create_tmp_file() {
    F_TEMP=$(tempfile 2>/dev/null) || F_TEMP=/tmp/test$$
    trap "rm -f $F_TEMP" 0 1 2 5 15
}

get_choice() {
    echo $(cat $F_TEMP)
}
