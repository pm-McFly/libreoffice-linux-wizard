low_get_src() {
    clear

    if [ ! -d "$1/.git" ]; then
        run_command git clone git://gerrit.libreoffice.org/core "$1"
    else
        cd "$1"
        run_command "git pull"
        cd -
    fi
}
