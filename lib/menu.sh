low_main_menu() {
    local OPTIONS=(
        1 "Install Deps"
        2 "Config Wizard"
        3 "Download Sources"
        4 "Build..."
        5 "Run..."
        6 "Quick Release Build"
        7 "Quick Debug Build"
        8 "About?"
        9 "[DEV] Refresh"
        0 "Quit!"
    )

    while :; do
        d_menu "Main Menu" "" "${OPTIONS[@]}"

        case $RESULT in
        0)
            local choice=$(get_choice)

            case $choice in
            0)
                break
                ;;
            2)
                low_config_menu
                ;;
            3)
                low_get_src "$WORKDIR"
                ;;
            1)
                install_deps
                ;;
            4)
                low_build_menu
                ;;
            5)
                low_run_menu
                ;;
            6)
                quick_rls_build
                ;;
            7)
                quick_dbg_build
                ;;
            8)
                about
                ;;
            9)
                load_libs
                ;;
            esac
            ;;
        *)
            break
            ;;
        esac
    done
}

low_config_menu() {
    local OPTIONS=(
        1 "Choose Dialog Dimensions"
        2 "Set Working Directory"
        3 "Configure ccache"
        4 "Configure autogen.input"
        5 "Copy vscode Config"
        0 "Reset!"
    )

    while :; do
        d_menu "Configuration Menu" "" "${OPTIONS[@]}"

        case $RESULT in
        0)
            local choice=$(get_choice)

            case $choice in
            1)
                low_size_menu
                ;;
            2)
                low_set_cwd
                ;;
            3)
                low_ccache_menu
                ;;
            4)
                low_autogen_menu
                ;;
            5)
                set_vscode_config
                ;;
            0)
                low_conf_reset
                ;;
            esac
            ;;
        *)
            break
            ;;
        esac
    done
}

low_size_menu() {
    local OPTIONS=(
        1 "McFly Auto Mode"
        2 "Dialog Auto (may not work)"
        3 "Dialog Max!"
    )

    while :; do
        d_menu "Dimensions Menu" "" "${OPTIONS[@]}"

        case $RESULT in
        0)
            local choice=$(get_choice)

            case $choice in
            1)
                low_set_size "\$((\$(tput lines) / (100 / 50)))" "\$((\$(tput cols) / (100 / 50)))"
                break
                ;;
            2)
                low_set_size 0 0
                break
                ;;
            3)
                low_set_size -1 -1
                break
                ;;
            esac
            ;;
        *)
            break
            ;;
        esac
    done
}

low_ccache_menu() {
    local OPTIONS=(
        1 "Change Cache Directory"
        2 "Change Cache Size"
        3 "Enable Cache Compression"
        0 "Write Config!"
    )

    while :; do
        d_menu "ccache Config Menu" "" "${OPTIONS[@]}"

        case $RESULT in
        0)
            local choice=$(get_choice)

            case $choice in
            1)
                low_set_cache_dir
                ;;
            2)
                low_set_cache_size
                ;;
            3)
                low_set_ccache_comp
                ;;
            0)
                low_write_ccache_conf
                break
                ;;
            esac
            ;;
        *)
            break
            ;;
        esac
    done
}

low_autogen_menu() {
    local OPTIONS=(
        1 "Load Default Debug Config"
        2 "Load Default Release Config"
        3 "Load Custom Config"
        4 "Edit Manually"
        0 "Reset!"
    )

    while :; do
        d_menu "autogen.input Config Menu" "Actual config:\n- Debug=$DBG_LOADED\n- Release=$RLS_LOADED\n- Custom=$CUS_LOADED" "${OPTIONS[@]}"

        case $RESULT in
        0)
            local choice=$(get_choice)

            case $choice in
            1)
                low_autogen_dbg_load
                ;;
            2)
                low_autogen_rls_load
                ;;
            3)
                low_autogen_manual_load
                ;;
            4)
                nano "$WORKDIR/autogen.input"
                set_loaded_vars 0 0 1
                ;;
            0)
                reset_autogen
                clean_clean
                ;;
            esac
            ;;
        *)
            break
            ;;
        esac
    done
}

low_build_menu() {
    local OPTIONS=(
        1 "Run autogen.sh"
        2 "Run Compilation"
        3 "Run Tests"
        4 "Clean..."
    )

    while :; do
        d_menu "Build Menu" "" "${OPTIONS[@]}"

        case $RESULT in
        0)
            local choice=$(get_choice)

            case $choice in
            1)
                run_autogen
                ;;
            2)
                build_run
                ;;
            3)
                build_tests
                ;;
            4)
                low_clean_menu
                ;;
            esac
            ;;
        *)
            break
            ;;
        esac
    done
}

low_clean_menu() {
    local OPTIONS=(
        1 "make clean"
        2 "make distclean"
    )

    while :; do
        d_menu "Clean Menu" "" "${OPTIONS[@]}"

        case $RESULT in
        0)
            local choice=$(get_choice)

            case $choice in
            1)
                clean_clean
                ;;
            2)
                clean_dist
                ;;
            esac
            ;;
        *)
            break
            ;;
        esac
    done
}

low_run_menu() {
    local OPTIONS=(
        1 "Run Debug Mode"
        2 "Run soffice"
        3 "Run Writer"
        4 "Run Calc"
        5 "Run Impress"
    )

    while :; do
        d_menu "Run Menu" "" "${OPTIONS[@]}"

        case $RESULT in
        0)
            local choice=$(get_choice)

            case $choice in
            1)
                low_run_debug
                ;;
            2)
                low_run ""
                ;;
            3)
                low_run "--writer"
                ;;
            4)
                low_run "--calc"
                ;;
            5)
                low_run "--impress"
                ;;
            esac
            ;;
        *)
            break
            ;;
        esac
    done
}
