low_run() {
    run_command "$WORKDIR/instdir/program/soffice" "$1"
}

low_run_debug() {
    d_msg "Run Debug Mode" "This will run the gdb debugger.\n\nTo start runnnig:\n- soffice: type \"run\"\n- soffice Writer: type \"run --writer\"\n- soffice Calc: type \"run --calc\""
    run_command "make -C" "$WORKDIR" "debugrun"
}
