low_set_cwd() {
    d_directory_select "Set Working Directory" "$WORKDIR"

    case $RESULT in
    0)
        WORKDIR=$(cat $F_TEMP)
        config_set "$USR_CFG_LOC" "WORKDIR" "$WORKDIR"
        ;;
    esac
}

low_set_size() {
    HEIGHT=$(eval echo $1)
    WIDTH=$(eval echo $2)

    config_set "$USR_CFG_LOC" "HEIGHT" "$1"
    config_set "$USR_CFG_LOC" "WIDTH" "$2"
}

low_set_cache_size() {
    d_range_box "Set Cache Directory" "\nPress \"Up\" to decrease\nPress \"Down\" to increase\n\nSize in Gb\nCurrently available $(df -h / --output=avail | tail -1)" 0 100 $CCACHE_MAX

    case $RESULT in
    0)
        CCACHE_MAX=$(cat $F_TEMP)
        config_set "$USR_CFG_LOC" "CCACHE_MAX" "$CCACHE_MAX"
        ;;
    esac
}

low_set_cache_dir() {
    d_directory_select "Set Cache Directory" "$CCACHE_DIR"

    case $RESULT in
    0)
        CCACHE_DIR=$(cat $F_TEMP)
        config_set "$USR_CFG_LOC" "CCACHE_DIR" "$CCACHE_DIR"
        ;;
    esac
}

low_set_ccache_comp() {
    d_yesno "Cache Compression" "Do you want to enable the cache compression system ?\nActual: $CCACHE_COMP"

    case $RESULT in
    0)
        CCACHE_COMP="true"
        config_set "$USR_CFG_LOC" "CCACHE_COMP" "$CCACHE_COMP"
        ;;
    1)
        CCACHE_COMP="false"
        config_set "$USR_CFG_LOC" "CCACHE_COMP" "$CCACHE_COMP"
        ;;
    esac
}

low_write_ccache_conf() {
    if [ ! -e "$CCACHE_FILE" ]; then
        mkdir -p "$CCACHE_CONF_DIR"
        touch "$CCACHE_FILE"
    fi

    CCACHE_BASEDIR="${WORKDIR}"
    echo "base_dir = ${CCACHE_BASEDIR}" >${CCACHE_FILE}
    echo "cache_dir = ${CCACHE_DIR}" >>${CCACHE_FILE}
    echo "max_size = ${CCACHE_MAX}" >>${CCACHE_FILE}
    echo "compression = ${CCACHE_COMP}" >>${CCACHE_FILE}
    d_prgbox "Write Conf" "cat ${CCACHE_FILE}"
}

low_conf_reset() {
    d_yesno "Reset Wizard Configuration" "WARNING!!\nThis action will erase your configuration and set back the default one.\n\nDo you really want to reset the $NAME?"

    case $RESULT in
    0)
        rm -rf $USR_CFG_LOC
        clean_dist
        reset_autogen
        load_config
        low_write_ccache_conf
        ;;
    esac
}
