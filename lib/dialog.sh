d_yesno() {
    dialog \
        --clear \
        --backtitle "$BACKTITLE" \
        --title "$1" \
        --yesno "$2" $HEIGHT $WIDTH
    RESULT=$?
    clear
}

d_input() {
    dialog \
        --clear \
        --backtitle "$BACKTITLE" \
        --title "$1" \
        --inputbox "$2" $HEIGHT $WIDTH \
        2>$F_TEMP
    RESULT=$?
    clear
}

d_menu() {
    local TITLE=$1
    shift
    local MSG=$1
    shift
    local OPTIONS=("$@")
    local CHOICE_HEIGHT=${#OPTIONS[@]}

    dialog \
        --clear \
        --backtitle "$BACKTITLE" \
        --title "$TITLE" \
        --menu "$MSG" $HEIGHT $WIDTH $CHOICE_HEIGHT "${OPTIONS[@]}" \
        2>$F_TEMP
    RESULT=$?
    clear
}

d_radio_menu() {
    local TITLE=$1
    shift
    local MSG=$1
    shift
    local OPTIONS=("$@")
    local CHOICE_HEIGHT=${#OPTIONS[@]}

    dialog \
        --clear \
        --backtitle "$BACKTITLE" \
        --title "$TITLE" \
        --radiolist "$MSG" $HEIGHT $WIDTH $CHOICE_HEIGHT "${OPTIONS[@]}" \
        2>$F_TEMP
    RESULT=$?
    clear
}

d_check_menu() {
    local TITLE=$1
    shift
    local MSG=$1
    shift
    local OPTIONS=("$@")
    local CHOICE_HEIGHT=${#OPTIONS[@]}

    dialog \
        --clear \
        --backtitle "$BACKTITLE" \
        --title "$TITLE" \
        --checklist "$MSG" $HEIGHT $WIDTH $CHOICE_HEIGHT "${OPTIONS[@]}" \
        2>$F_TEMP
    RESULT=$?
    clear
}

d_file_select() {
    dialog \
        --clear \
        --backtitle "$BACKTITLE" \
        --title "$1" \
        --fselect "$2" $HEIGHT $WIDTH \
        --stdout \
        >$F_TEMP
    RESULT=$?
    clear
}

d_directory_select() {
    dialog \
        --clear \
        --backtitle "$BACKTITLE" \
        --title "$1" \
        --dselect "$2" $HEIGHT $WIDTH \
        --stdout \
        >$F_TEMP
    RESULT=$?
    clear
}

d_prgbox() {
    dialog \
        --clear \
        --backtitle "$BACKTITLE" \
        --title "$1" \
        --prgbox "$2" $HEIGHT $WIDTH
    RESULT=$?
    clear
}

d_range_box() {
    dialog \
        --clear \
        --backtitle "$BACKTITLE" \
        --title "$1" \
        --rangebox "$2" $HEIGHT $WIDTH \
        $3 $4 $5 \
        --stdout \
        >$F_TEMP
    RESULT=$?
    clear
}

d_msg() {
    dialog \
        --clear \
        --backtitle "$BACKTITLE" \
        --title "$1" \
        --msgbox "$2" $HEIGHT $WIDTH
    RESULT=$?
    clear
}
