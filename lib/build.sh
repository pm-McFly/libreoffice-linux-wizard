build_run() {
    d_yesno "Build \"build-nocheck\"" "Do you want to check while building?"

    case $RESULT in
    0)
        run_command "make -C $WORKDIR"
        ;;
    *)
        run_command "make -C $WORKDIR build-nocheck"
        ;;
    esac
}

build_tests() {
    run_command "make -C $WORKDIR check"
}

build_custom() {
    run_command "make -C $WORKDIR" "$@"
}