quick_dbg_build() {
    if [[ $DBG_LOADED != 1 ]]; then
        low_autogen_dbg_load
        run_autogen
    fi
    build_run
}

quick_rls_build() {
    if [[ $RLS_LOADED != 1 ]]; then
        low_autogen_rls_load
        run_autogen
    fi
    build_run
}