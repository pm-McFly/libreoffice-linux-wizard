config_read_file() {
  (grep -E "^${2}=" -m 1 "${1}" 2>/dev/null || echo "VAR=__UNDEFINED__") | head -n 1 | cut -d '=' -f 2-
}

config_get() {
  val="$(config_read_file "${USR_CFG_LOC}" "${1}")"

  if [ "${val}" = "__UNDEFINED__" ]; then
    val="$(config_read_file "${DFT_CFG_LOC}" "${1}")"
  fi

  printf -- "%s" "$(eval echo $val)"
}

config_set() {
  local file=$1
  local key=$2
  local val=${@:3}

  is_file_exist "${file}"

  # create key if not exists
  if ! grep -q "^${key}=" ${file}; then
    # insert a newline just in case the file does not end with one
    printf "\n${key}=" >>${file}
  fi

  chc "$file" "$key" "$val"
}

is_file_exist() {
  if [ ! -e "$1" ]; then
    if [ -e "$1.example" ]; then
      cp "$1.example" "$1"
    else
      touch "$1"
    fi
  fi
}

chc() {
  gawk -v OFS== -v FS== -e 'BEGIN { ARGC = 1 } $1 == ARGV[2] { print ARGV[4] ? ARGV[4] : $1, ARGV[3]; next } 1' "$@" <"$1" >"$1.1"
  mv "$1"{.1,}
}

load_config() {
  DFT_CFG_LOC=".config/defaults.cfg"
  USR_CFG_LOC=".config/usr.cfg"

  SLEEP_TIME=2
  RESULT=0
  CORES=$(nproc)
  PARALELLISM="--with-parallelism=$CORES"
  DBG_CFG=".config/debug_autogen.input"
  RLS_CFG=".config/release_autogen.input"
  VSCODE_FLD=".config/.vscode/"

  DBG_LOADED=$(config_get DBG_LOADED)
  RLS_LOADED=$(config_get RLS_LOADED)
  CUS_LOADED=$(config_get CUS_LOADED)

  AUTHOR=$(config_get AUTHOR)
  LICENSE=$(config_get LICENSE)
  VERSION=$(config_get VERSION)
  NAME=$(config_get NAME)

  PROMT=$(config_get PROMT)

  HEIGHT=$(config_get HEIGHT)
  WIDTH=$(config_get WIDTH)
  BACKTITLE=$(config_get BACKTITLE)

  WORKDIR=$(config_get WORKDIR)

  CCACHE_DIR=$(config_get CCACHE_DIR)
  CCACHE_CONF_DIR=$(config_get CCACHE_CONF_DIR)
  CCACHE_BASEDIR=$(config_get CCACHE_BASEDIR)
  CCACHE_MAX=$(config_get CCACHE_MAX)
  CCACHE_FILE=$(config_get CCACHE_FILE)
  CCACHE_COMP=$(config_get CCACHE_COMP)
}

set_loaded_vars() {
  DBG_LOADED=$1
  RLS_LOADED=$2
  CUS_LOADED=$3

  config_set "$USR_CFG_LOC" "DBG_LOADED" "$DBG_LOADED"
  config_set "$USR_CFG_LOC" "RLS_LOADED" "$RLS_LOADED"
  config_set "$USR_CFG_LOC" "CUS_LOADED" "$CUS_LOADED"
}

set_vscode_config() {
  d_prgbox "Copy vscode config file" "cp -r $VSCODE_FLD $WORKDIR && echo done!"
}