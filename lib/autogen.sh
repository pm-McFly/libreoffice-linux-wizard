low_autogen_default_load() {
    echo $PARALELLISM >"$WORKDIR/autogen.input"
    cat $1 >>"$WORKDIR/autogen.input"
}

low_autogen_dbg_load() {
    low_autogen_default_load $DBG_CFG
    clean_clean
    set_loaded_vars 1 0 0
}

low_autogen_rls_load() {
    low_autogen_default_load $RLS_CFG
    clean_clean
    set_loaded_vars 0 1 0
}

low_autogen_manual_load() {
    local OPTIONS=(
        "with-parallelism=${CORES}" "" OFF
        "with-system-dicts" "" OFF
        "with-myspell-dicts" "" OFF
        "with-system-zlib" "" OFF
        "with-linker-hash-style=both" "" OFF
        "with-fonts" "" OFF
        "with-external-thes-dir=/usr/share/mythes" "" OFF
        "with-external-hyph-dir=/usr/share/hyphen" "" OFF
        "with-external-dict-dir=/usr/share/hunspell" "" OFF
        "without-junit" "" OFF
        "without-java" "" OFF
        "without-help" "" OFF
        "without-doxygen" "" OFF
        "without-krb5" "" OFF
        "without-gssapi" "" OFF
        "without-system-poppler" "" OFF
        "without-system-openssl" "" OFF
        "without-system-libpng" "" OFF
        "without-system-libxml" "" OFF
        "without-system-jpeg" "" OFF
        "without-system-jars" "" OFF
        "without-system-postgresql" "" OFF
        "without-helppack-integration" "" OFF
        "disable-odk" "" OFF
        "disable-assert-always-abort" "" OFF
        "disable-gstreamer-1-0" "" OFF
        "disable-gtk3" "" OFF
        "disable-dconf" "" OFF
        "disable-werror" "" OFF
        "disable-compiler-plugins-analyzer-pch" "" OFF
        "disable-dependency-tracking" "" OFF
        "disable-split-debug" "" OFF
        "disable-firebird-sdbc" "" OFF
        "enable-debug" "" OFF
        "enable-dbgutil" "" OFF
        "enable-release-build" "" OFF
        "enable-mergelibs" "" OFF
        "enable-compiler-plugins" "" OFF
        "enable-option-checking=fatal" "" OFF
    )

    d_check_menu "Custom Manual autogen.input" "Choose 0 or more options.\n\nUse [SPACE] to select\nUse [ENTER] and \"Left\" & \"Right\" to choose OK or Cancel" "${OPTIONS[@]}"

    case $RESULT in
    0)
        local OPT="--"$(cat "$F_TEMP" | sed -r "s/ /\n--/g")
        if [ "$OPT" != "--" ]; then
            echo "$OPT" >"$WORKDIR/autogen.input"
        else
            echo "" >"$WORKDIR/autogen.input"
        fi
        clean_clean
        ;;
    esac
    set_loaded_vars 0 0 1
}

reset_autogen() {
    d_yesno "Reset autogen Configuration" "WARNING!!\nThis action will erase any configuration.\n\nDo you really want to erase it?"

    case $RESULT in
    0)
        rm -f "$WORKDIR/autogen.input"
        ;;
    esac
    set_loaded_vars 0 0 0
}

run_autogen() {
    cd $WORKDIR
    run_command "./autogen.sh"
    cd -
}
