about() {
    d_msg "About - $NAME" \
        "\n\n$NAME\n$VERSION\n$LICENSE\n\n\
This fantastic script has been meticulously designed\n\
to facilitate the precise configuration\n\
of the development environment for all those\n\
who passionately want to work proactively on\n\
the Libre Office project.\n\n
THIS IS NOT AN OFFICIAL SCRIPT !!"
}
